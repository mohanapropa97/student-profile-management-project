package com.example.java_assignment_2.service;

import com.example.java_assignment_2.dto.StudentDto;
import com.example.java_assignment_2.entity.Address;
import com.example.java_assignment_2.entity.ParentInfo;
import com.example.java_assignment_2.entity.Student;
import com.example.java_assignment_2.repository.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public String saveStudent(StudentDto studentDto) {

          Student student = dtoToEntity(studentDto);
            studentRepository.save(student);
            return "Saved Successfully";
    }

/*--------------------------*Helper Method*-------------------------------*/


    private Student dtoToEntity(StudentDto studentDto){
        Student student= new Student();
        BeanUtils.copyProperties(studentDto,student);

        Address address = new Address();
        BeanUtils.copyProperties(studentDto.getAddressDto(),address);
        student.setAddress(address);

        ParentInfo parentInfo = new ParentInfo();
        BeanUtils.copyProperties(studentDto.getParentInfoDto(),parentInfo);
        student.setParentInfo(parentInfo);

        return student;
    }
}