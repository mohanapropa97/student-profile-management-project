package com.example.java_assignment_2.repository;

import com.example.java_assignment_2.entity.AcademicInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface AcademicInfoRepository extends JpaRepository<AcademicInfo,Long> {

}
