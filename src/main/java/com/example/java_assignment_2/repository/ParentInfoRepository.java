package com.example.java_assignment_2.repository;

import com.example.java_assignment_2.entity.ParentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParentInfoRepository extends JpaRepository<ParentInfo,Long> {

}
