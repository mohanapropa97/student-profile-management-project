package com.example.java_assignment_2.repository;

import com.example.java_assignment_2.entity.ClassInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ClassInfoRepository extends JpaRepository<ClassInfo,Long> {

    boolean existsByClassName(int className);
}
