package com.example.java_assignment_2.init;

import com.example.java_assignment_2.entity.ClassInfo;
import com.example.java_assignment_2.repository.ClassInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor

public class ClassinfoInitializer implements CommandLineRunner {

    private final ClassInfoRepository classInfoRepository;

    @Override
    public void run(String... args) throws Exception {
        List<ClassInfo> classInfoList = Arrays.asList(new ClassInfo(1,5,"Propa"),
                new ClassInfo(2,5,"Mohana"),
                new ClassInfo(3,5,"Aradhya"),
                new ClassInfo(4,6,"Nymhpaea"),
                new ClassInfo(5,8,"Lily"));

        List<ClassInfo> classInfoList2 = classInfoList.stream().filter(classinfo -> !classInfoRepository.existsByClassName(classinfo.getClassName())).toList();

        classInfoRepository.saveAll(classInfoList2);

    }
}
