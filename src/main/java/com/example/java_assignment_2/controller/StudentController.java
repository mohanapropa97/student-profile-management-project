package com.example.java_assignment_2.controller;

import com.example.java_assignment_2.dto.StudentDto;
import com.example.java_assignment_2.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
@AllArgsConstructor

public class StudentController {

    private final StudentService studentService;

    @PostMapping("/save")
    public ResponseEntity<?> saveEmployee(@RequestBody StudentDto studentDto) {
        String message = studentService.saveStudent(studentDto);
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }
}
