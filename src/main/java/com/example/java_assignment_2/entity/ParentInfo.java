package com.example.java_assignment_2.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class ParentInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String fatherName;
    @Column
    private String fatherNID;
    @Column
    private String fatherNumber;
    @Column
    private String motherName;
    @Column
    private String motherNID;
    @Column
    private String motherNumber;
}
