package com.example.java_assignment_2.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Data
@Entity
@DynamicInsert
@DynamicUpdate
public class AcademicInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String UID;
    @Column
    private String admissionDate;
    @Column
    private double sscGrade;
    @Column
    private double hscGrade;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "academicInfo_classInfo",
            joinColumns = @JoinColumn(name = "academicInfo_id"),
            inverseJoinColumns = @JoinColumn(name = "classInfo_id"))
    private ClassInfo classInfo;

}
