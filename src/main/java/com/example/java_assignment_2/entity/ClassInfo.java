package com.example.java_assignment_2.entity;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
@Data
@Entity
@DynamicInsert
@DynamicUpdate

public class ClassInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private int className;
    @Column
    private int numberOfSubject;
    @Column
    private String classTeacherName;

    public ClassInfo(int className, int numberOfSubject, String classTeacherName) {
        this.className = className;
        this.numberOfSubject = numberOfSubject;
        this.classTeacherName = classTeacherName;
    }
}
