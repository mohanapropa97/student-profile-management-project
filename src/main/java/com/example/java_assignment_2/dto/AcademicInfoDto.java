package com.example.java_assignment_2.dto;

import lombok.Data;

@Data


public class AcademicInfoDto {
    private long id;
    private String UID;
    private String admissionDate;
    private double sscGrade;
    private double hscGrade;
    private ClassinfoDto classinfoDto;

}
