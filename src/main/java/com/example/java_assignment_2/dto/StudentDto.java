package com.example.java_assignment_2.dto;
import lombok.Data;

@Data
public class StudentDto {
    private long id;
    private String name;
    private String gender;
    private String phoneNumber;
    private String email;
    private String dateOfBirth;
    private AddressDto addressDto;
    private ParentInfoDto parentInfoDto;
    private AcademicInfoDto academicInfoDto;

}
