package com.example.java_assignment_2.dto;

import lombok.Data;

@Data

public class ClassinfoDto {

    private long id;
    private int className;
    private int numberOfSubject;
    private String classTeacherName;

}
