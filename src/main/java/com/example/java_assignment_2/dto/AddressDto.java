package com.example.java_assignment_2.dto;
import lombok.Data;

@Data
public class AddressDto {
    private long id;
    private String city;
    private String area;
    private String residenceDetails;
}
