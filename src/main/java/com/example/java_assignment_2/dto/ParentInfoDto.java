package com.example.java_assignment_2.dto;

import lombok.Data;

@Data
public class ParentInfoDto {

    private long id;
    private String fatherName;
    private String fatherNID;
    private String fatherNumber;
    private String motherName;
    private String motherNID;
    private String motherNumber;
}
